<?php
namespace App\Services;


use App\Mail\email;
use Illuminate\Support\Facades\Mail;

class emailService implements sendInterface
{
    private $message;
    private $recipient;
    private $mailer;

    public function __construct(Mail $mailer)
    {
    $this->mailer = $mailer;
    }

    public function setMessage(string $message)
    {
        $this->message = $message;
    }

    public function setRecipient(string $recipient)
    {
        $this->recipient = $recipient;
    }

    public function sendMessage()
    {
        $this->mailer::to($this->recipient)->send(new email($this->message));
    }


}