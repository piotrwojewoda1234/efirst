<?php
namespace App\Services;

use SMSApi\Client;
use SMSApi\Api\SmsFactory;
use SMSApi\Exception\SmsapiException;

class smsService implements sendInterface
{
    private $message;
    private $recipient;
    private $smsApi;


    public function __construct()
    {
     $client = Client::createFromToken(env('SMS_TOKEN'));
     $factory = new SmsFactory();
     $this->smsApi = $factory->setClient($client);
    }

    public function setMessage(string $message)
    {
   $this->message = $message;
    }

    public function setRecipient(string $recipient)
    {
    $this->recipient = $recipient;
    }

    public function sendMessage()
    {
        try {
            $actionSend = $this->smsApi->actionSend();

            $actionSend->setTo($this->recipient);
            $actionSend->setText($this->message);
            $actionSend->setSender('sender');
            $response = $actionSend->execute();

            foreach ($response->getList() as $status) {
                echo $status->getNumber() . ' ' . $status->getPoints() . ' ' . $status->getStatus();
            }
        } catch (SmsapiException $exception) {
            echo 'ERROR: ' . $exception->getMessage();
        }
    }


}