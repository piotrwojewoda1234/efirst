<?php
namespace App\Services;

interface sendInterface {
    public function setMessage(string $message);
    public function setRecipient(string $recipient);
    public function sendMessage();
}