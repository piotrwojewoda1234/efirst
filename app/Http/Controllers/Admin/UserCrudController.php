<?php

namespace App\Http\Controllers\Admin;

use App\Services\emailService;
use App\Services\smsService;

use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Http\Requests\CrudRequest;
use Backpack\PermissionManager\app\Http\Requests\UserStoreCrudRequest as StoreRequest;
use Backpack\PermissionManager\app\Http\Requests\UserUpdateCrudRequest as UpdateRequest;
use Backpack\PermissionManager\app\Http\Controllers\UserCrudController as OriginalUserCrudController;
use Illuminate\Foundation\Auth\User;
use Illuminate\Support\Facades\Mail;
use App\Page;
class UserCrudController extends OriginalUserCrudController
{
    public $emailService;
    public $smsService;

    public function __construct(emailService $emailService,smsService $smsService)
    {
    parent::__construct();
    $this->emailService = $emailService;
    $this->smsService = $smsService;
    }

    public function setup()
    {
      parent::setup();
        $this->crud->addButton('line', 'send_email', 'view', 'crud::buttons.send', 'end');
        $this->crud->addButton('line', 'send_sms', 'view', 'crud::buttons.sendsms', 'end');
    }

    public function sendmail(int $id){
        $user = User::find($id);

        $this->emailService->setMessage("e-mail message");
        $this->emailService->setRecipient($user->email);
        $this->emailService->sendMessage();
    }

    public function sendsms() {
        $this->emailService->setMessage("sms message");
        $this->emailService->setRecipient('514006001');
        $this->smsService->sendMessage();
    }

}
