<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class email extends Mailable
{
    use Queueable, SerializesModels;

    protected $value;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($value)
    {
       $this->value = $value;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.email')->with('value',$this->value);
    }
}
